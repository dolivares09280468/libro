<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="Style.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Insertar</title>
    </head>
    <body>
        <div id="header">
            <h1>Insertar</h1>
            <hr>
        </div>
        <div id="cuerpo1">
            <form action="ClienteWeb.jsp" method="POST">
                <table align="center">
                    <tr>
                        <td>
                            Introduzca el titulo:
                        </td>
                        <td>
                            <input type="text" name="t1" value="" size="30"/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Introduzca el nombre del autor:
                        </td>
                        <td>
                            <input type="text" name="aut" value="" size="30"/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Introduzca el precio:
                        </td>
                        <td>
                            <input type="text" name="precio" value="" size="30"/><br>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <br/>
                            <input type="submit" value="Crear"/>
                        </td>
                        <td align="left">
                            <br/>
                            <input type="reset" value="Limpiar"/>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
