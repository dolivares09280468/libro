<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="Style.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar</title>
    </head>
    <body>
        <div id="header">
            <h1>Editar</h1>
            <hr>
        </div>
        <div id="body">
            <%
                String s0=new String(request.getParameter("id").getBytes());
                String s1=new String(request.getParameter("t1").getBytes());
                String s2=new String(request.getParameter("aut").getBytes());
                String s3=new String(request.getParameter("precio").getBytes());
                System.out.println("s1="+s1);
            %>
            <form action="ClienteWeb2.jsp" method="POST">
                <table align="center">
                    <tr>
                        <td>
                            <input type="hidden" name="id" value="<%=s0%>" readonly="readonly" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Introduzca el titulo:
                        </td>
                        <td>
                             <input type="text" name="t1" value="<%=s1%>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Introduzca el nombre del autor:
                        </td>
                        <td>
                             <input type="text" name="aut" value="<%=s2%>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Introduzca el precio:
                        </td>
                        <td>
                             <input type="text" name="precio" value="<%=s3%>" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <input type="submit" value="Guardar"/>
                        </td>
                        <td align="left">
                            <input type="reset" value="Limpiar"/>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>