package Stateless;
import Entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
@Stateless
public class LibroBean implements LibroBeanRemote
{
    @PersistenceContext(name="Libreria-ejbPU")
    EntityManager em;
    Libro libro;
    Collection <Libro> listaLibros;
    @Override
    public void addLibro(String titulo, String autor, BigDecimal precio) 
    {
        if(libro==null)
        {
            libro=new Libro(titulo,autor,precio);
            em.persist(libro);
            libro=null;
        }
    }
    @Override
    public Collection<Libro> getAllLibro()
    {
        listaLibros=em.createNamedQuery("Libro.findAll").getResultList();
        return listaLibros;
    }   
    @Override
    public Libro buscaLibro(int id)
    {
        libro=em.find(Libro.class, id);
        /*
        System.out.println("Regresando Libro");
        if(libro!=null)
            System.out.println("Libro hallado "+libro.getTitulo());
        else
            System.out.println("Libro no hallado");
        */
        return libro;
    }
    @Override
    public void actualizaLibro(Libro libro, String Titulo, String autor, BigDecimal precio)
    {
        if(libro!=null)
        {
            System.out.println("Actualizando libro");
            libro.setTitulo(Titulo);
            libro.setAutor(autor);
            libro.setPrecio(precio);
            em.merge(libro);
        }
    }
    public void eliminaLibro(int id)
    {
        libro=em.find(Libro.class, id);
        if(libro!=null)
        {
            System.out.println("Eliminando Registro");
            em.remove(libro);
            libro=null;
        }
    }
}